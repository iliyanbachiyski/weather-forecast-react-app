import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import React from "react";
import Forecast from "../../entities/Forecast";

interface OwnProps {
  item: Forecast;
}

const HourlyCard: React.FC<OwnProps> = function ({ item }) {
  return (
    <Card>
      <CardContent className="contentContainer">
        <Typography variant="subtitle1" color="GrayText" textAlign="center">
          {item.date.toLocaleTimeString([], {
            hour: "2-digit",
            minute: "2-digit",
          })}
        </Typography>
        <Typography variant="subtitle2" textAlign="center">
          {item.description}
        </Typography>
        <img
          src={`http://openweathermap.org/img/wn/${item.icon}@2x.png`}
          alt={item.description}
        />
        <Typography variant="h3" textAlign="center" gutterBottom>
          {item.temperature}&#176;
        </Typography>
      </CardContent>
    </Card>
  );
};

export default HourlyCard;
