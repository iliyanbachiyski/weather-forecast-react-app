import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import React from "react";
import Forecast from "../../entities/Forecast";
import HourlyCard from "../HourlyCard";
import "./style.css";

interface OwnProps {
  items: Forecast[];
}

const DailyCard: React.FC<OwnProps> = function ({ items }) {
  const mainItem = items[0];
  const hourlyItems = items.slice(1, items.length);
  return (
    <Card>
      <CardContent className="contentContainer">
        <img
          src={`http://openweathermap.org/img/wn/${mainItem.icon}@2x.png`}
          alt={mainItem.description}
        />
        <Typography variant="body1">
          <b>{mainItem.description}</b>
        </Typography>
        <Typography variant="subtitle1">{mainItem.city}</Typography>
        <Typography variant="subtitle1" color="GrayText" gutterBottom>
          {mainItem.date.toLocaleDateString()}
        </Typography>
        <Typography variant="h3" gutterBottom>
          {mainItem.temperature}&#176;
        </Typography>
        <div className="minMaxContainer">
          <Typography variant="body2" gutterBottom>
            Min: {mainItem.minTemperature}&#176;
          </Typography>
          &nbsp; | &nbsp;
          <Typography variant="body2" gutterBottom>
            Max: {mainItem.maxTemperature}&#176;
          </Typography>
        </div>
        <div className="additionalInfoContainer">
          <div>
            <Typography variant="subtitle1" color="GrayText">
              Humidity
            </Typography>
            <Typography variant="subtitle2" textAlign="center">
              {mainItem.humidity}%
            </Typography>
          </div>
          <div>
            <Typography variant="subtitle1" color="GrayText">
              Pressure
            </Typography>
            <Typography variant="subtitle2" textAlign="center">
              {mainItem.pressure} hPa
            </Typography>
          </div>
          <div>
            <Typography variant="subtitle1" color="GrayText">
              Wind speed
            </Typography>
            <Typography variant="subtitle2" textAlign="center">
              {mainItem.windSpeed} km/h
            </Typography>
          </div>
        </div>
        <div className="hourlyItemsContainer">
          {hourlyItems.map((el) => (
            <div key={el.date.valueOf()} className="hourlyItemContainer">
              <HourlyCard item={el} />
            </div>
          ))}
        </div>
      </CardContent>
    </Card>
  );
};

export default DailyCard;
