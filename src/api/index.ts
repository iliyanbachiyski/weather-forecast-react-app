import { OPEN_WEATHER_API_KEY } from "../config";
import Forecast, { parse } from "../entities/Forecast";

export interface ForecastResponse {
  [day: number]: Forecast[];
}

export const fetchWeatherForecastByName = function (name: string) {
  return fetchWeatherForecast(
    `https://api.openweathermap.org/data/2.5/forecast?q=${name}&units=metric&appid=${OPEN_WEATHER_API_KEY}`
  );
};

export const fetchWeatherForecastByLocation = function (
  latitude: number,
  longitude: number
) {
  return fetchWeatherForecast(
    `https://api.openweathermap.org/data/2.5/forecast?lat=${latitude}&units=metric&lon=${longitude}&appid=${OPEN_WEATHER_API_KEY}`
  );
};

export const fetchWeatherForecast = function (url: string) {
  return new Promise<ForecastResponse>((resolve, reject) => {
    fetch(url)
      .then((res) => res.json())
      .then(
        (response) => {
          if (response.cod !== "200") {
            reject(response.message);
          } else {
            const forecastList: Forecast[] = response.list.map((el: any) => {
              const item = parse(el);
              item.city = `${response.city.name}, ${response.city.country}`;
              return item;
            });
            const result: ForecastResponse = {};
            forecastList.forEach((el) => {
              const day = el.date.getDate();
              if (day in result) {
                result[day] = [...result[day], el];
              } else {
                result[day] = [el];
              }
            });
            resolve(result);
          }
        },
        (error) => {
          reject(error);
        }
      );
  });
};
