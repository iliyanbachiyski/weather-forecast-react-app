interface Forecast {
  city: string;
  temperature: number;
  minTemperature: number;
  maxTemperature: number;
  feelsLikeTemperature: number;
  humidity: number;
  pressure: number;
  windSpeed: number;
  icon: string;
  description: string;
  date: Date;
}

export const parse = function (data: any): Partial<Forecast> {
  return {
    temperature: Math.round(data.main.temp),
    minTemperature: Math.round(data.main.temp_min),
    maxTemperature: Math.round(data.main.temp_max),
    feelsLikeTemperature: data.main.feels_like,
    humidity: data.main.humidity,
    pressure: data.main.pressure,
    windSpeed: data.wind.speed,
    icon: data.weather[0].icon,
    description: data.weather[0].main,
    date: new Date(data.dt_txt),
  };
};

export default Forecast;
