import Alert from "@mui/material/Alert";
import AlertTitle from "@mui/material/AlertTitle";
import Button from "@mui/material/Button";
import CircularProgress from "@mui/material/CircularProgress";
import Collapse from "@mui/material/Collapse";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import React, { useCallback, useState } from "react";
import {
  fetchWeatherForecastByLocation,
  fetchWeatherForecastByName,
  ForecastResponse,
} from "./api";
import DailyCard from "./components/DailyCard";

function App() {
  const [city, setCity] = useState<string>("");
  const [error, setError] = useState<string>("");
  const [loading, setLoading] = useState<boolean>(false);
  const [forecastResponse, setForecastResponse] = useState<ForecastResponse>();

  const handleForecastResponse = useCallback((response: ForecastResponse) => {
    setForecastResponse(response);
    setLoading(false);
  }, []);

  const handleForecastError = useCallback((error: any) => {
    if (typeof error === "string") {
      setError(error);
    } else {
      setError("Something went wrong!");
    }
    setLoading(false);
  }, []);

  const handleFetchData = useCallback(() => {
    setLoading(true);
    handleAlertClose();
    fetchWeatherForecastByName(city)
      .then(handleForecastResponse)
      .catch(handleForecastError);
  }, [city, handleForecastResponse, handleForecastError]);

  const handleGetCurrentLocation = useCallback(() => {
    setCity("");
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          setLoading(true);
          handleAlertClose();
          fetchWeatherForecastByLocation(
            position.coords.latitude,
            position.coords.longitude
          )
            .then(handleForecastResponse)
            .catch(handleForecastError);
        },
        (error) => {
          setError(error.message);
        }
      );
    } else {
      setError("Geolocation is not supported by this browser!");
    }
  }, [navigator.geolocation, handleForecastResponse, handleForecastError]);

  const handleCityChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setCity(event.target.value);
    },
    []
  );
  const handleAlertClose = useCallback(() => {
    setError("");
  }, []);

  return (
    <Container maxWidth="sm">
      <Typography align="center" variant="h5" gutterBottom>
        Welcome to the Forecast Application!
      </Typography>
      <Grid container spacing={2} alignItems="center">
        <Grid item xs={8}>
          <TextField
            value={city}
            size="small"
            variant="outlined"
            label="City"
            fullWidth
            onChange={handleCityChange}
            InputProps={{
              endAdornment: loading ? <CircularProgress size={20} /> : null,
            }}
          />
        </Grid>
        <Grid item xs={4}>
          <Button
            fullWidth
            variant="contained"
            color="primary"
            disabled={!city}
            onClick={handleFetchData}
          >
            Search
          </Button>
        </Grid>
        <Grid item xs={12}>
          <Button
            fullWidth
            variant="outlined"
            color="secondary"
            onClick={handleGetCurrentLocation}
          >
            Get current location
          </Button>
        </Grid>
        <Grid item xs={12}>
          <Collapse in={!!error}>
            <Alert severity="error" onClose={handleAlertClose}>
              <AlertTitle>Error</AlertTitle>
              {error}
            </Alert>
          </Collapse>
        </Grid>
      </Grid>
      {!!forecastResponse && (
        <Grid container spacing={2}>
          {Object.keys(forecastResponse).map((day) => (
            <Grid key={day} item xs={12}>
              <DailyCard items={forecastResponse[Number(day)]} />
            </Grid>
          ))}
        </Grid>
      )}
    </Container>
  );
}

export default App;
