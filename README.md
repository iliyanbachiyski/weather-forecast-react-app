# Weather forecast application

It allows the users to:

- search by city
- use their current location
- to see the next 5 days weather forecast.
- to see the weather forecast hourly

The project is using Material UI to define the visual components and the main layout.
The project is using TypeScript.

# Improvement notes

- Implement autocomplete text field to easily find the location.
- Adding more weather information.
- Implement tests

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm install`

**Important!**
**Please run this command before starting the application. It will install all the necessary dependencies.**

### `npm start`

Once you have all dependencies installed, run the app in the development mode with the following command.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.
